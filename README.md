## ci-functions.sh

### Usage

```
before_script:
  - source <(curl -s https://gitlab.com/seerkan/autoops/raw/master/ci-functions.sh)
  - generate_env


scripts:
  - build
```

### generate_env

It will generate a .env file with the content of (in this priority order):
  - .env.default
  - all the env vars that start with FLAG_ and CONFIG_ ( if REACT_APP=true is set, all the env vars will get REACT_APP_ appended)
  - .env.test if the CI_JOB_STAGE == "test"
  
### notify_slack

  - SLACK_TOKEN env var needs to be defined
  - call the function in script with: ```- slack_message="@here This is a slack notification" notify_slack```

### build

The command will login to docker hub (if DOCKERHUB_USER is defined), or to gitlab registry (if CI_REGISTRY_USER is defined, should be by default if you are using gitlab ci).
If a Dockerfile is found, it will build with ENV_BRANCH var and it will tag and push with the branch tag + CI_COMMIT_SHA

If you are using docker hub, please make sure you have these env vars configured:
  - $DOCKERHUB_USER
  - $DOCKERHUB_PASSWORD
  - $DOCKERHUB_REPO
