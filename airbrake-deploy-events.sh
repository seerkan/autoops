#!/bin/bash

PROJECT_ID=${AIRBRAKE_PROJECT_ID}
PROJECT_KEY=${AIRBRAKE_PROJECT_KEY}
ENVIRONMENT=${CI_COMMIT_REF_NAME}
REPOSITORY=${CI_PROJECT_URL}
REVISION="$(git rev-parse HEAD)"
USERNAME=$(GITLAB_USER_EMAIL)

curl -X POST \
  -H "Content-Type: application/json" \
  -d '{"environment":"'${ENVIRONMENT}'","username":"'${USERNAME}'","repository":"'${REPOSITORY}'","revision":"'${REVISION}'"}' \
  "https://airbrake.io/api/v4/projects/${PROJECT_ID}/deploys?key=${PROJECT_KEY}"